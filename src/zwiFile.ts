import { ZWIMetadata } from "./zwiMetadata";
import { readFileSync } from "node:fs";
import { validate } from "typia";
import * as fflate from "fflate";
import { UnzipOptions } from "fflate";
import { getSHA1Hash, textDecoder } from "./utils";

export const ZWI_VERSION = 1.4;

export class ZWIFile {
    files: Record<string, Uint8Array>;

    private _metadata?: ZWIMetadata;
    get metadata(): ZWIMetadata {
        if (!this._metadata) {
            const metadata = this.getFileAsJSON("metadata.json");
            if (!metadata) throw new Error("Invalid ZWI file: Missing metadata.json");
            this._metadata = metadata;
            return metadata;
        }
        return this._metadata;
    }

    constructor(files: Record<string, Uint8Array>) {
        this.files = files;
    }

    static async fromURL(input: RequestInfo, init?: RequestInit): Promise<ZWIFile> {
        return this.fromUint8Array(new Uint8Array(await (await fetch(input, init)).arrayBuffer()));
    }

    static fromFile(path: string): ZWIFile {
        return this.fromUint8Array(readFileSync(path));
    }

    static fromUint8Array(data: Uint8Array, opts?: UnzipOptions): ZWIFile {
        return new ZWIFile(fflate.unzipSync(data, opts));
    }

    getFileAsString(name: string): string | undefined {
        const file = this.files[name];
        if (!file) return;
        return textDecoder.decode(file);
    }

    getFileAsJSON(name: string): any {
        const file = this.getFileAsString(name);
        if (!file) return;
        return JSON.parse(file);
    }

    /**
     * Validates this {@link ZWIFile}.
     * @returns `valid`: Indicates whether the ZWI file is valid.<br>
     *          `signature`: Info about the person/organization that signed the ZWI file. Only present if the ZWI file is valid and `skipSignatureVerification` is `false`.<br>
     *          `errors`: An array of human-readable errors. Only present if the ZWI file is invalid.
     */
    validate(skipSignatureVerification?: boolean): Promise<Identity | undefined | string[]> {
        return new Promise((resolve, reject) => {
            const errors: string[] = [];


            //
            // Validate metadata.json
            //

            const metadata = this.metadata;
            if (typeof metadata !== "object") {
                errors.push(`typeof metadata is wrong. Expected object, got ${typeof metadata}`);
            } else {
                const result = validate<ZWIMetadata>(metadata);
                if (!result.success) {
                    for (const error of result.errors) {
                        // slice(7) removes "$input." from the beginning
                        errors.push(`Invalid field in metadata.json: ${error.path.slice(7)}. Expected ${error.expected}, got ${error.value}`);
                    }
                }
            }

            const filenames = Object.keys(this.files);
            const validateHashes = (
                filenames: string[],
                hashes: Record<string, string>,
                validating: string
            ): void => {
                for (const filename of filenames) {
                    const expected = getSHA1Hash(this.files[filename]);
                    const actual = hashes[filename];
                    if (expected !== actual) errors.push(`Invalid hash in ${validating}: ${filename}. Expected ${expected}, got ${actual}`);
                }
            };

            // Validate Content field in metadata
            validateHashes(
                filenames.filter(f => !f.startsWith("data/") && f !== "metadata.json" && f !== "media.json" && f !== "signature.json"),
                metadata.Content,
                "metadata.json (Content field)"
            );


            //
            // Validate media.json
            //

            if (filenames.some(f => f.startsWith("data/"))) { // Checks if files exist in the data folder
                try {
                    const media = this.getFileAsJSON("media.json");
                    if (!media) {
                        errors.push('Files are present in the "data" directory, but media.json is not present');
                    } else if (typeof media !== "object") {
                        errors.push(`typeof media is wrong. Expected object, got ${typeof media}`);
                    } else {
                        validateHashes(filenames.filter(f => f.startsWith("data/")), media, "media.json");
                    }
                } catch (e) {
                    errors.push(`Error parsing media.json${e instanceof Error ? `: ${e.message}` : "."}`);
                }
            }


            //
            // Verify signature
            //

            let signature: Identity | undefined;
            if (!skipSignatureVerification) {
                const signature = this.getFileAsJSON("signature.json");
                if (signature) {
                    let url = signature.url;
                    url = url.replace("did:psqr:", "https://");
                    if (url.includes("#")) url = url.slice(0, url.indexOf("#"));
                    url += "/.well-known/psqr/";
                    const response = await fetch(url);
                    const json = await response.json();

                }
            }


            //
            // Return results
            //

            if (errors.length > 0) reject(errors);
            else resolve(signature);
        });
    }

    async sign(identity: Identity) {

    }
}

type Identity = {
    /** The name of a person or organization. */
    name: string

    /** The address of a person or organization. Can be a physical address (not necessarily exact), email address, or URL. */
    address: string
};
