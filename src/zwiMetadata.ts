import { ZWI_VERSION } from "./zwiFile";

export interface ZWIMetadata {

    //
    // Required fields
    //

    /** The version of the ZWI specification the article adheres to */
    ZWIversion: typeof ZWI_VERSION

    /** The original title of the article, with no modifications */
    Title: string

    /** The ISO 639-1 2-letter language code representing the language the article is in ([list of codes](https://www.sitepoint.com/iso-2-letter-language-codes)) */
    Lang: string

    /** A list of article files and their SHA-1 hashes */
    Content: Record<string, string>

    /** The ID of the person or organization that published the article. Publisher IDs must be lowercase with no spaces. */
    Publisher: string

    /** The human-readable name of the encyclopedia the article belongs to */
    CollectionTitle: string

    /** The last time the ZWI file was modified, in seconds since the Unix epoch */
    LastModified: string

    /** The time the ZWI file was created, in seconds since the Unix epoch */
    TimeCreated: string

    /** The first ~350 characters of the article */
    Description: string

    /** The article's license */
    License: string

    /** The URL of the original source of the article */
    SourceURL: string

    //
    // Optional fields
    //

    /** A list of categories. No attempt is made to standardize categories. */
    Categories?: string[]

    /** The main/preferred article file. If there are no article files, this field should be left blank. */
    Primary?: string

    /** A list of people that created the article. Can be real names or usernames. */
    Authors?: string[]

    /** A list of people that worked on the article */
    Editors?: string[]

    /** A list of people that created the article. Can be real names or usernames. */
    CreatorNames?: string[]

    /** A list of people that worked on the article */
    ContributorNames?: string[]

    /** A list of topics */
    Topics?: string[]

    /** A short description of the article */
    ShortDescription?: string

    /** Additional information about the article itself, not the article's topic */
    Comment?: string

    /** Currently unused. In the future, this field will be used to implement a rating system. */
    Rating?: Array<unknown>

    /** Currently unused */
    Revisions?: Array<unknown>

    /** The name of the software that was used to generate the ZWI file */
    GeneratorName?: string

    /** The number of words in the article */
    WordCount?: number

    /** The date the article was originally published, in `yyyy-mm-dd` format */
    PublicationDate?: string

    /** The namespace the article is in */
    Namespace?: string

    /** A simplified title, with things like disambiguators and namespaces removed. Inverted titles are also de-inverted. */
    ShortTitle?: string

    /** Some text describing what differentiates this article from articles with the same title. For example, in "God (sculpture)", the `Disambiguator` is `"sculpture"`. */
    Disambiguator?: string

    /** An array of alternative titles. For example, the `AlternativeTitles` for "Bed; Bedchamber; Bedstead" would be `["Bed", "Bedchamber", "Bedstead"]`. */
    AlternativeTitles?: string[]

    /** Whether the `Title` of the article is inverted, e.g. "God, Image Of" */
    InvertedTitle?: boolean

    /** Whether the article is a redirect */
    Redirect?: boolean

    /** The URL that the article is redirecting to */
    RedirectURL?: string

    /** Whether the article is cross-referencing another article */
    CrossReference?: boolean

    /** The title of the article this article is cross-referencing */
    CrossReferenceTitle?: string

    /** Whether the article is a stub (under 350 characters) */
    Stub?: boolean

    /** Whether the article is an actual article */
    IsArticle?: boolean
}
