import { existsSync, lstatSync, mkdirSync } from "node:fs";

export class ZWIDatabase {
    private readonly _root: string;
    get root(): string {
        return this._root;
    }

    constructor(root: string) {
        this._root = root;
        if (existsSync(this._root)) {
            if (!lstatSync(this._root).isDirectory()) throw new Error("Invalid ZWI database: Root path is not a directory");
        } else {
            mkdirSync(this._root, { recursive: true });
        }
    }
}
