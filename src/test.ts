import { ZWIFile } from "./zwiFile";

const times: number[] = [];
for (let i = 0; i < 1000; i++) {
    const start = performance.now();
    const metadata = ZWIFile.fromFile("wiki#Knowledge.zwi").metadata;
    times.push(performance.now() - start);
}
console.log(times.reduce((a, b) => a + b) / times.length);

/*(async () => {
    const file = await ZWIFile.fromURL("https://encyclosearch.org/encyclosphere/database/en%2Fconservapedia%2Fwww.conservapedia.com%2FKnowledge.zwi");
    for (const [filename, content] of (await file?.files) ?? []) {
        console.log(filename, await content.text());
    }
})();*/

/*const file = ZWIFile.fromFile("wiki#Knowledge.zwi");
console.log(file.validate());*/

