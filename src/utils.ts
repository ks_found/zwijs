import * as crypto from "crypto";

export const textDecoder = new TextDecoder();

export function getSHA1Hash(data: Uint8Array): string {
    return crypto.createHash("sha1").update(data).digest("hex");
}
